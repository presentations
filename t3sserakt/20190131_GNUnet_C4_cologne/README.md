This Presentation is build with reveal.js

* Download the latest version of reveal.js from https://github.com/hakimel/reveal.js/releases
Unzip and replace the example contents in index.html with the index.html from this directory.
* Open index.html in a browser to view it

The Presentation is in german.

Please have a look into the ERRATA.md and ADDENDUM.md files.