There was a questioner after the talk who assumed, that in a project like GNUnet with a lot of code written in a university context, the code might stay in an "old" status, when it was written.

This might be true in parts, but important components like the used crypto primitives and other parts of the code are subject to continuous improvement. Since the beginning of GNUnet there was also a extensive refactoring of the code base.
